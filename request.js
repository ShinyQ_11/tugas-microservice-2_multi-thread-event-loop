const https = require('https')
const start = Date.now()

function doRequest(){
    https
    .request('https://shumi.shop/', res => {
        res.on('data', () => {});
        res.on('end', () => {
            console.log(Date.now() - start);
        })
    })
    .end()
}

doRequest();
doRequest();
doRequest();
doRequest();
doRequest();
doRequest();
doRequest();