process.env.UV_THREADPOOL_SIZE = 5

const https = require('https')
const crypto = require('crypto')
const fs = require('fs')

const start = Date.now()

function doRequest(){
    try{
        https
        .request('https://shumi.shop/', res => {
            res.on('data', () => {});
            res.on('end', () => {
                console.log("Request:", Date.now() - start);
            })
        })
        .end()
    } catch(err){
        console.log(err);
    }
}

function doHash(){
    crypto.pbkdf2('a', 'b', 100000, 512, 'sha512', () => {
        console.log('Hash:', Date.now() - start);
    })
}

doRequest()

fs.readFile('multitask.js', 'utf-8', () => {
    console.log('FS:', Date.now() - start)
})

doHash()
doHash()
doHash()
doHash()